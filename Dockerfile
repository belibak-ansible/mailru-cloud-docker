FROM debian:9.4

RUN apt-get update
RUN apt-get install -y \
python \
wget \
sudo \
xvfb \
x11vnc \
 gnupg \
 libqt5widgets5 \
 libqt5x11extras5

RUN echo "mailru ALL=NOPASSWD:/usr/bin/Xvfb" > /etc/sudoers.d/mailru \
&& echo "mailru ALL=NOPASSWD:/bin/rm" >> /etc/sudoers.d/mailru
RUN useradd -ms /bin/bash mailru

ADD mail.ru-cloud_15.06.0110_amd64.deb /usr/local/src/mailru.deb
RUN dpkg -i /usr/local/src/mailru.deb
ADD start.sh /usr/local/sbin/start.sh

USER mailru
